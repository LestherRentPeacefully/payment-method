"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var admin = require("firebase-admin");
var environment_1 = require("../environments/environment");
var Database = (function () {
    function Database() {
        this.config = {
            credential: admin.credential.cert(environment_1.environment.firebaseConfig.credential),
            databaseURL: environment_1.environment.firebaseConfig.databaseURL
        };
    }
    Database.prototype.init = function () {
        admin.initializeApp(this.config);
    };
    return Database;
}());
exports.Database = Database;
