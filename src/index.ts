
import bodyParser = require('body-parser');
import cors = require('cors');
import * as express from 'express';


/*Database initialization */
import {Database} from "./services/Database";

let database = new Database();
database.init();

// Routes
import {AltcoinRoutes} from "./routes/AltcoinRoutes";
import {EthereumRoutes} from "./routes/EthereumRoutes";
import {ListenerRoutes} from './routes/ListenerRoutes';

// Services
import {Listener as ETH} from "./services/Ethereum/Listener";
import {Listener as Altcoin} from "./services/Altcoin/Listener";


// Express server
const app = express();

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());



// Cors Requests Security
let whitelist: any[] = [];
let corsOptions: any = {};



app.use(cors(corsOptions));


/*
* Set Port
* */

const PORT = process.env.PORT || 8080;


app.get('/', (req, res) => {
  res.send('Hello world :)');
});


app.use('/altcoin', AltcoinRoutes);

app.use('/ethereum', EthereumRoutes);

app.use('/listener', ListenerRoutes);


const ETHlistener = new ETH();
ETHlistener.watchForDeposits();


const LTCListener = new Altcoin('LTC');
LTCListener.watchForDeposits();

const BTCListener = new Altcoin('BTC');
BTCListener.watchForDeposits();

const DASHListener = new Altcoin('DASH');
DASHListener.watchForDeposits();

const KODListener = new Altcoin('KOD');
KODListener.watchForDeposits();



// Start up the Node server
app.listen(PORT, () => {
  console.log(`Node server listening on http://localhost:${PORT}`);
});
