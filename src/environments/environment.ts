
/*
* Environment Variable to be exported
* */


let serviceAccount = require("../../serviceAccountKey.json");

const firebaseConfig ={
    credential: serviceAccount,
    databaseURL: "https://onbtcio-43288.firebaseio.com"
};

export const environment  = {
    firebaseConfig: firebaseConfig,
    server:{
      // api:'http://bitxmi.com/api/cointoken',
      api:'http://5.189.177.23/api/cointoken'
    },
    blockchain:{
        ETH:{
            nodeURL:'http://localhost:8545',
            listenerTimer:13*1000,
            securePlatformWallet:{
                address:'0x4e2538bb44620b51c4609578aac710c5697fd876',
                privateKey:'0191cca4357160bcf050e710c210f2d7ba732640593e181e32555910e13724ca'
            },
        },
        LTC:{
            nodeURL:'http://localhost:9332', 
            listenerTimer:2.5*60*1000,
            securePlatformWallet:{
                address:'Lhxenx27Q6FivVSbciPSjzYHYAJj6eo8DD',
                privateKey:'9de1c15e406c3f161c737c6cbde6429c33598981bbaa0625294a433430c34512'
            },
            network:{ //main
                messagePrefix: '\x19Litecoin Signed Message:\n',
                bip32: {
                  public: 0x019da462,
                  private: 0x019d9cfe
                },
                pubKeyHash: 0x30,
                scriptHash: 0x32,
                wif: 0xb0
            }
        },
        BTC:{
            nodeURL:'http://localhost:8332',
            listenerTimer:10*60*1000,
            securePlatformWallet:{
                address:'1AkC2xwPnQsSFhwv3saur9hWe5yuuEitd9',
                privateKey:'437cacf694dbc3652f36ff9f7b3af7f11e938323e0b7889f0db14abc6926b43b',
            },
            network:{ //main
                messagePrefix: '\x18Bitcoin Signed Message:\n',
                bech32: 'bc',
                bip32: {
                    public: 0x0488b21e,
                    private: 0x0488ade4
                },
                pubKeyHash: 0x00,
                scriptHash: 0x05,
                wif: 0x80
            },
            
        },
        DASH:{
            nodeURL:'http://localhost:9998',
            listenerTimer:2.5*60*1000,
            securePlatformWallet:{
                address:'XkTNaLG1CQyHU8YemvU6Hg4W5Xxuo7YMVS',
                privateKey:'01506548ff47dd9e4b4269961181d59fe4c0524826530b9a5473434d2e8a51ba'
            },
            network:{ //mainnet
                messagePrefix: '\x19Dash Signed Message:\n',
                bip32: {
                  public: 0x0488b21e,
                  private: 0x0488ade4
                },
                pubKeyHash: 0x4c,
                scriptHash: 0x10,
                wif: 0xcc
            }
        },
        KOD:{
            nodeURL:'http://localhost:7857',
            listenerTimer:1*60*1000,
            securePlatformWallet:{
                address:'RDqhovVeyNCAa91rfH3Kzc47jFZZTmmW5x',
                privateKey:'1ec0e76e4cf153e1bc07e22d69463611797d74162bf2ae28a0e1f1ece5bebd65'
            },
            network:{ //mainnet
                messagePrefix: '\x19Kodcoin Signed Message:\n',
                bip32: {
                  public: 0x022d2533,
                  private: 0x0221312b
                },
                pubKeyHash: 0x3c,
                scriptHash: 0x1e,
                wif: 0x0f
            }
        },

    },
    tokens:[
        {
            name:'BNB',
            symbol:'BNB',
            decimals:18,
            contractAddress:'0xB8c77482e45F1F44dE1745F52C74426C631bDD52',
            contractABI:require("../../ABI.json")
        },
        {
            name:'VeChain Token',
            symbol:'VEN',
            decimals:18,
            contractAddress:'0xD850942eF8811f2A866692A623011bDE52a462C1', //0xD850942eF8811f2A866692A623011bDE52a462C1 //Real
            contractABI:require("../../ABI.json")
        },
        {
            name:'OMGToken',
            symbol:'OMG',
            decimals:18,
            contractAddress:'0xd26114cd6EE289AccF82350c8d8487fedB8A0C07', //0xd26114cd6EE289AccF82350c8d8487fedB8A0C07 /Real
            contractABI:require("../../ABI.json")
        },
        {
            name:'0x Protocol Token',
            symbol:'ZRX',
            decimals:18,
            contractAddress:'0xE41d2489571d322189246DaFA5ebDe1F4699F498',
            contractABI:require("../../ABI.json")
        },
        {
            name:'Basic Attention Token',
            symbol:'BAT',
            decimals:18,
            contractAddress:'0x0D8775F648430679A709E98d2b0Cb6250d2887EF',
            contractABI:require("../../ABI.json")
        },
        {
            name:'Pundi X Token',
            symbol:'NPXS',
            decimals:18,
            contractAddress:'0xA15C7Ebe1f07CaF6bFF097D8a589fb8AC49Ae5B3',
            contractABI:require("../../ABI.json")
        },
        {
            name:'ChainLink Token',
            symbol:'LINK',
            decimals:18,
            contractAddress:'0x514910771AF9Ca656af840dff83E8264EcF986CA',
            contractABI:require("../../ABI.json")
        },
        {
            name:'Aurora',
            symbol:'AOA',
            decimals:18,
            contractAddress:'0x9ab165D795019b6d8B3e971DdA91071421305e5a',
            contractABI:require("../../ABI.json")
        },
        {
            name:'TrueUSD',
            symbol:'TUSD',
            decimals:18,
            contractAddress:'0x8dd5fbCe2F6a956C3022bA3663759011Dd51e73E',
            contractABI:require("../../ABI.json")
        },
        {
            name:'HoloToken',
            symbol:'HOT',
            decimals:18,
            contractAddress:'0x6c6EE5e31d828De241282B9606C8e98Ea48526E2',
            contractABI:require("../../ABI.json")
        },
        {
            name:'Decentraland MANA',
            symbol:'MANA',
            decimals:18,
            contractAddress:'0x0F5D2fB29fb7d3CFeE444a200298f468908cC942',
            contractABI:require("../../ABI.json")
        },
        {
            name:'Status Network Token',
            symbol:'SNT',
            decimals:18,
            contractAddress:'0x744d70FDBE2Ba4CF95131626614a1763DF805B9E',
            contractABI:require("../../ABI.json")
        },
        {
            name:'HuobiToken',
            symbol:'HT',
            decimals:18,
            contractAddress:'0x6f259637dcD74C767781E37Bc6133cd6A68aa161',
            contractABI:require("../../ABI.json")
        },
        {
            name:'ODEM Token',
            symbol:'ODEM',
            decimals:18,
            contractAddress:'0xbf52F2ab39e26E0951d2a02b49B7702aBe30406a',
            contractABI:require("../../ABI.json")
        },
        {
            name:'IOSToken',
            symbol:'IOST',
            decimals:18,
            contractAddress:'0xFA1a856Cfa3409CFa145Fa4e20Eb270dF3EB21ab',
            contractABI:require("../../ABI.json")
        },
        {
            name:'Walton Token',
            symbol:'WTC',
            decimals:18,
            contractAddress:'0xb7cB1C96dB6B22b0D3d9536E0108d062BD488F74',
            contractABI:require("../../ABI.json")
        },
        {
            name:'Insight Chain',
            symbol:'INB',
            decimals:18,
            contractAddress:'0x17Aa18A4B64A55aBEd7FA543F2Ba4E91f2dcE482',
            contractABI:require("../../ABI.json")
        },
        {
            name:'Nexo',
            symbol:'NEXO',
            decimals:18,
            contractAddress:'0xB62132e35a6c13ee1EE0f84dC5d40bad8d815206',
            contractABI:require("../../ABI.json")
        },
        {
            name:'Oyster Pearl',
            symbol:'PRL',
            decimals:18,
            contractAddress:'0x1844b21593262668B7248d0f57a220CaaBA46ab9',
            contractABI:require("../../ABI.json")
        },
        {
            name:'Bancor Network Token',
            symbol:'BNT',
            decimals:18,
            contractAddress:'0x1F573D6Fb3F13d689FF844B4cE37794d79a7FF1C',
            contractABI:require("../../ABI.json")
        },
                    //Tokens on testnet for testing Purposes
        // {
        //     name:'Top5 VeChain Token',
        //     symbol:'TestVEN',
        //     decimals:18,
        //     contractAddress:'0xeA8018B7526E3BccBa8E8c9B2752D9934CbA2d56',
        //     contractABI:require("../../ABI.json")
        // },
        // {
        //     name:'Top5 OMGToken',
        //     symbol:'TestOMG',
        //     decimals:18,
        //     contractAddress:'0xF19E321C1dd8AD058353ef4e6c1d852408af644b',
        //     contractABI:require("../../ABI.json")
        // },                 
     ],

    // contractAddress:'0xeD5d9FCE6324443C03A1108DA5b3E41cc59949Ec',
    contractABI: require("../../ABI.json")


    
}; 



